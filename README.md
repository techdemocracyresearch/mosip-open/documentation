# Documentation
MOSIP is built considering a facade of architecture and design principles that makes it truly modular, interoperable, customizable and extensible.  This enables countries to adopt MOSIP as the kernel for building their foundational identity systems.   

We encourage you to first get an [overview](../..//wiki) of the MOSIP project and its [principles and architectural goals](../../wiki/Architecture-Principles-&-Platform-Goals).

The [logical architecture](../../wiki/Logical-Architecture) covers the key design aspects.

The [technology stack](../../wiki/Technology-Stack) lists some of the key technologies used in MOSIP.


## MOSIP Platform Documentation
Has everything that you need in terms of requirements, architecture, functional view and APIs.  
The document can be accessed [here](../../wiki/Platform-Documentation).

## MOSIP Developer Documentation
Contains guidelines, and instructions for the developer community.  
The document can be accessed [here](../../wiki/Developer-Documentation).

## MOSIP Tester Documentation
Contains useful information for the tester community.  
The document can be accessed [here](../../wiki/Tester-Documentation)

## Contributing to documentation
Coming soon...
